# lotto.py
# Programmer: Demar Brown (Demar Codes)
# Date: Dec 27, 2020
# Program Details: This program simulates the lotto.
    #1. The program displays a jackpot, payout guide and asks the user for 6 numbers ranging from 1 - 48.
    #2. The program then displays 6 random numbers from the same range and checks how many numbers the user guessed
        # correctly.
    #3. Allows the user to re-enter the lotto
    # 4. Show the user payout each round.

# 

#-----------------------------------------------

#Importing python's random library
import random

#This funcion determines the jackpot for each draw
def jackpot():
    jackpot = random.randrange(50000000, 250000000, 150000)

    jackpot_dict = {
        "six_matches": jackpot,
        "five_matches": jackpot * 0.01,
        "four_matches": jackpot * 0.005,
        "three_matches": jackpot * 0.001,
        "two_or_less_matches": 1000.0
    }

    print("\nToday's Jackpot for matching all 6 numbers in any order is a whopping $"+ str(jackpot_dict.get("six_matches")))
    print("Other prizes are as follow: ")
    print("5 Matches = $"+ str(jackpot_dict.get("five_matches")))
    print("4 Matches = $"+ str(jackpot_dict.get("four_matches")))
    print("3 Matches = $"+ str(jackpot_dict.get("three_matches")))
    print("2 or less Matches = $"+ str(jackpot_dict.get("two_or_less_matches"))+ "\n")

    return jackpot_dict

#This function generates the winning numbers for each draw
def generate_lotto_num():
    winning_numbers= []

    for i in range(0, 6):
        randomnum = random.randint(1, 48)
        while randomnum in winning_numbers:
            randomnum = random.randint(1, 48)

        winning_numbers.append(randomnum)    
    
    print("\nWinning numbers for this draw were: ")
    print(winning_numbers)
    return winning_numbers
    

#This function allows the user to pick his/her guesses
def user_choices():
    user_numbers = []
    for num in range(0,6):
        user_num = int(input(f'Enter number (between 1-48) choice {num+1}: '))
        while user_num > 48:
            print("Number out of range. Try Again!")
            user_num = int(input(f'Enter number (between 1-48) choice {num+1}: '))

            while user_num in user_numbers:
                print("Number already selected. Try Again!")
                user_num = int(input(f'Enter number (between 1-48) choice {num+1}: '))
        
        user_numbers.append(user_num)

    print("\nThe following numbers have been confirmed for you in this lotto draw: ")
    print(user_numbers)
    return user_numbers

#This function determines the payout per draw to user based on winning numbers and user picks
def payout(total_jackpot, lucky_numbers, user_picks):
    correct_guesses = 0
    draw_pay = 0

    for choice in user_picks:
        if choice in lucky_numbers:
            correct_guesses+=1
            
    if correct_guesses == 6:
        draw_pay = total_jackpot.get("six_matches")
    elif correct_guesses == 5:
        draw_pay = total_jackpot.get("five_matches")
    elif correct_guesses == 4:
        draw_pay = total_jackpot.get("four_matches")
    elif correct_guesses == 3:
        draw_pay = total_jackpot.get("three_matches")
    elif correct_guesses <= 21:
        draw_pay = total_jackpot.get("two_or_less_matches")

    print("\nYou guessed " + str(correct_guesses) + " numbers correctly")
    print("Your payout for this draw is $" + str(draw_pay) + "\n")

    return draw_pay



#Program begins here

print("\n\t\t--- WELCOME TO--- \n\t~THE GREAT WESTERN LOTTO DRAW~--- \n")

status = True
accumulated_payout = 0
play_lotto = int(input("Do you wish to play? - 1 for Yes, 2 for No :"))


if play_lotto == 1:
    status = True
elif play_lotto == 2:
    status = False

while status == True:
    total_jackpot = jackpot()
    user_picks = user_choices()
    lucky_numbers = generate_lotto_num()
    draw_payout = payout(total_jackpot, lucky_numbers, user_picks)
    accumulated_payout+= draw_payout
    
    play_lotto = int(input("Do you wish to play again? - 1 for Yes, 2 for No :"))

    if play_lotto == 1:
        status= True
    elif play_lotto == 2:
        status = False
        print(f"\nThank you for playing. Your total payout today was ${accumulated_payout}. See you soon!")

